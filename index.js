var express=require('express');
var app=express();
var path=require('path');
var logger=require('morgan');
var bodyParser=require('body-parser');
var TSNE=require('tsne-js');
var node_xj = require("xls-to-json");
var neo4j = require('neo4j-driver').v1;

let model = new TSNE({
  dim: 2,
  perplexity: 30.0,
  earlyExaggeration: 4.0,
  learningRate: 100.0,
  nIter: 1000,
  metric: 'euclidean'
});

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use(express.static(path.join(__dirname,'public')));

const driver = neo4j.driver('bolt://localhost',neo4j.auth.basic('neo4j','graph'));

const session = driver.session();
function checker(dt){

  if(dt<10)
  {
      var ds="0"+dt;
      return ds;
  }
  return dt;
}
function checker2(ti){
  //console.log(ti);
  if(ti<10)
  {
      var ts="0"+ti.toString();
      //console.log(ts);
      return ts;
  }
  return ti;
}
app.get('/',function(req,res){
  dates=[]
  values=[]
  dates2=[]
  values2=[]
  var tccounter
  session.run('match(n:equipment{name:"TC1"}),(b:equipment{name:60001}),(n)-[r:monitors]->(b) return apoc.date.format(r.date,"s","dd-mm-yyyy"),apoc.date.format(r.time,"s","HH:mm:ss") order by r.date,r.time')
         .then(function(result){

            result.records.forEach(function(record){

                var date=new Date(record._fields[0]+" "+record._fields[1]);
                var dater=date.getFullYear()+"-"+checker((date.getMonth()+1))+"-"+checker(date.getDate())+" "+checker2(date.getHours())+":"+checker2(date.getMinutes())+":"+checker2(date.getSeconds());
                var datet=dater;
                //console.log(dater+"  "+values);
                dates.push(datet);
                //values.push(value)
            })


            session.run('match(n:equipment),(b:equipment{name:60001}),(n)-[r:monitors]->(b) return n.name,r.value order by n.name,r.date,r.time')
                   .then(function(result){
                     var count=0;
                     var last="";
                     var val=[];
                     var record2=[];
                      result.records.forEach(function(record){
                          if(count==0)
                          {
                              last=record._fields[0];
                              val.push(record._fields[1]);
                              count=count+1;
                          }
                          else {

                                if(last==record._fields[0])
                                {
                                  val.push(record._fields[1]);
                                }
                                else {
                                  record2.push({
                                    name:last,
                                    value:val
                                  });
                                  last=record._fields[0];
                                  //console.log(record2);
                                  val=[];
                                  count=count+1;
                                }


                          }

                      })
                      //console.log(record2);
                      //res.send({rec:record2});
                      res.render('index',{date:dates,rec:record2});
                });


            //console.log(dates);

         });
         //console.log(dates);

	var inputData=[[10,16], [10,5], [10,11], [9,9],[8,12],[8,11],[7,9],[7,8]];
	model.init({
  data: inputData,
  type: 'dense'
});

// `error`,  `iter`: final error and iteration number
// note: computation-heavy action happens here
let [error, iter] = model.run();

// rerun without re-calculating pairwise distances, etc.
let [error2, iter2] = model.rerun();

// `output` is unpacked ndarray (regular nested javascript array)
let output = model.getOutput();
console.log(output);
// `outputScaled` is `output` scaled to a range of [-1, 1]
let outputScaled = model.getOutputScaled();

console.log(outputScaled);
	//res.render('index',{output:outputScaled});
});



app.listen(4000);

app.set('views',path.join(__dirname,'views'));
app.set('view engine','ejs');
